/**
 * System configuration for Angular samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
    System.config({
        paths: {
            // paths serve as alias
            'src-npm:': 'src/node_modules/'
          },
      // packages tells the System loader how to load when no filename and/or no extension
        packages: {
           "dist": {
               "defaultExtension" : "js",
              "main" : "main"
            }
        },
        map:  {
            "lodash": "src-npm:lodash/lodash.js",
            // "lodash" : "https://npmcdn.com/lodash@4.17.4"
        }
    });
    System.import( "dist" );
  })(this);
  