"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var two_1 = require("./two");
var _ = require("lodash");
new two_1.Two();
var App = /** @class */ (function () {
    function App() {
        this.title = "MyApp";
        console.log("Loading main app!!!!");
    }
    App.prototype.getUsers = function () {
        console.log("App::getUsers called");
        return [{ "name": "John" }];
    };
    return App;
}());
var x = new App();
console.log(_.isArray(x.getUsers()));
//# sourceMappingURL=main.js.map