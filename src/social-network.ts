import { Person } from "./person";

export interface SocialNetwork {
    title: String;
    getUsers(): Person[];
};

