import { Two } from "./two";
import { SocialNetwork } from './social-network';
import * as _ from 'lodash';

new Two();

class App implements SocialNetwork {
    title   = "MyApp";
  constructor() {
     
      console.log( "Loading main app!!!!");
  }

  getUsers () {
      console.log("App::getUsers called");
      return [{"name": "John"}] ;
  }
}

var x = new App();

console.log( _.isArray(x.getUsers()) );
